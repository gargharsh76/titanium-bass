# DBMS-Project #

### How to contribute to the project: ###

* Fork this repo.
* Create a branch in your Fork.
* Push changes to your branch.
* Once you want others to see and review your changes, create a pull request.
* We'll go through the pull request and merge them into the master project.

### Authors: ###
* Durga Akhil Mundroy (01FB15ECS097)
* Harsh Garg (01FB15ECS118)
* Ganesh K. (01FB15ECS104)